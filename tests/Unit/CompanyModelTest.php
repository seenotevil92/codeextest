<?php

namespace Test\Unit ;

use App\Company\Models\Company;
use App\Company\Parser\DaData\RequestConstructor;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Tests\TestCase;

class CompanyModelTest extends TestCase {

    public function testCreateCompany()
    {
        $testFaker = [
            'name' => 'МОСКОВСКОЕ ПРЕДСТАВИТЕЛЬСТВО ФИРМЫ ТЕЛЕ2 РОССИЯ ИНТЕРНЕШНЛ СЕЛЛУЛАР Б.В. (НИДЕРЛАНДЫ)',
            'ogrn' => '20150019164',
            'inn' => '9909005588',
            'kpp' => '773851001',
            'address' => 'НИДЕРЛАНДЫ',
            'fio' => 'Эмдин Сергей Владимирович',
            'gid' => Str::uuid()
        ] ;

        $company = Company::create($testFaker) ;
        $this->assertInstanceOf(Company::class, $company) ;
        $this->assertTrue($company->id > 0) ;
    }

    public function testGuzzleHttp()
    {

        $client = new Client() ;
        $config = include public_path() . '/../config/dadata.php';

        $requestConstructor = new RequestConstructor() ;
        $requestConstructor->setApiKey($config['api-key']) ;
        $requestConstructor->setQuery("9909005588") ;
        $request = $requestConstructor->request() ;

        $response = $client->request($request->getMethod(), $request->getUri(), [
            'verify' => false,
            'headers' => $request->getHeaders(),
            'body' => $request->getBody()

        ]) ;
        $this->assertEquals(200, $response->getStatusCode()) ;

    }

}