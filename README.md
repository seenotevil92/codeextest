# codeextest

Выполнение тестового задания от codeex

## Описание API  
Api работает по протоколу HTTP/1.1  
Запросы принимаются в двух видах в виде json c заголовком application/json и в виде POST запроса с заголовком application/x-www-form-urlencoded.   

В случае положительного ответа возвращает Http Code 200 и рпезультат если он есть в виде json:  
В случае ошибки возвращает Http Code 404: и тело ошибки в виде json с ключом error {"error" => data}      

### Список команд   
1. Read:  
   Request:  
       Method: GET  
       Url: api/company/read  
   Response:
       Content-Type: application/json   
       Body: 
       [{  
                     "id":22,  
                "gid":"85c2e1f4-24bd-460a-8796-b1149a34e3c7",  
                "name":"МОСКОВСКОЕ ПРЕДСТАВИТЕЛЬСТВО ФИРМЫ ТЕЛЕ2 РОССИЯ ИНТЕРНЕШНЛ СЕЛЛУЛАР Б.В. (НИДЕРЛАНДЫ)",
                "ogrn":"20150019164",  
                "inn":"9909005588",  
                "kpp":"773851001",  
                "address":"НИДЕРЛАНДЫ",  
                "fio":"Эмдин Сергей Владимирович"  
            },  
            {  
            "id":23,
            "gid":"08b1b4e1-e3f1-4e89-b98d-d4aad693e848",  
            "name":"МОСКОВСКОЕ ПРЕДСТАВИТЕЛЬСТВО ФИРМЫ ТЕЛЕ2 РОССИЯ ИНТЕРНЕШНЛ СЕЛЛУЛАР Б.В. (НИДЕРЛАНДЫ)",  
            "ogrn":"20150019164",  
            "inn":"9909005588",  
            "kpp":"773851001",  
            "address":"НИДЕРЛАНДЫ",  
            "fio":"Эмдин Сергей Владимирович"  
        }]  
2. Create:  
   Request:
       Method: POST  
       Url: api/company/create 
       Body:   
       {  
            "name":"МОСКОВСКОЕ ПРЕДСТАВИТЕЛЬСТВО ФИРМЫ ТЕЛЕ2 РОССИЯ ИНТЕРНЕШНЛ СЕЛЛУЛАР Б.В. (НИДЕРЛАНДЫ)",  
            "ogrn":"20150019164",  
            "inn":"9909005588",  
            "kpp":"773851001",  
            "address":"НИДЕРЛАНДЫ",  
            "fio":"Эмдин Сергей Владимирович"   
       } 
   Response:  
       Http Code: 200  
       Content-Type: application/json 
3. Update:  
   Request:
       Method: POST  
       Url: api/company/update/{id} - id - номер компании 
       Body:   
       {  
            "name":"МОСКОВСКОЕ ПРЕДСТАВИТЕЛЬСТВО ФИРМЫ ТЕЛЕ2 РОССИЯ ИНТЕРНЕШНЛ СЕЛЛУЛАР Б.В. (НИДЕРЛАНДЫ)",  
            "ogrn":"20150019164",  
            "inn":"9909005588",  
            "kpp":"773851001",  
            "address":"НИДЕРЛАНДЫ",  
            "fio":"Эмдин Сергей Владимирович" 
       } 
   Response:  
       Http Code: 200  
       Content-Type: application/json 
4. Remove:  
   Request:  
       Method: POST    
       Url: api/company/remove/{id} - id номер компании для удаления  
   Response: 
       Http code: 200  
       Content-Type: application/json 
5. Destroy:  
   Request:  
       Method: POST    
       Url: api/company/destroy/{id} - id номер компании безвозвратного для удаления  
   Response: 
       Http code: 200  
       Content-Type: application/json 

6. Compose:  
   Request:  
       Method: POST  
       Url: api/company/compose  
       Body:   
       {  
            "name":"МОСКОВСКОЕ ПРЕДСТАВИТЕЛЬСТВО ФИРМЫ ТЕЛЕ2 РОССИЯ ИНТЕРНЕШНЛ СЕЛЛУЛАР Б.В. (НИДЕРЛАНДЫ)",  
            "ogrn":"20150019164",  
            "inn":"9909005588",  
            "kpp":"773851001  
       } 
   Response: 
       Http code 200 
       Body:    
       {  
            "name":"МОСКОВСКОЕ ПРЕДСТАВИТЕЛЬСТВО ФИРМЫ ТЕЛЕ2 РОССИЯ ИНТЕРНЕШНЛ СЕЛЛУЛАР Б.В. (НИДЕРЛАНДЫ)",  
            "ogrn":"20150019164",  
            "inn":"9909005588",  
            "kpp":"773851001",  
            "address":"НИДЕРЛАНДЫ",  
            "fio":"Эмдин Сергей Владимирович" 
       }
     
7. Export:  
   Request:  
       Method: POST  
       Url: api/company/export  
   Response: 
       Http code: 200  
       Content-Type: application/octet-stream  
       Body: binary    

8. Import:  
   Request:  
       Method: POST  
       Url: api/company/import  
       Body: [import_file => binary]   
   Response: Http code 200  
  
 
### Развертывание приложения  
1) Запускаем docker-compose up --build  - для сборки окружения   
2) Запускам их корня сайта команду php artisan migrate - для созданий нобходимых таблиц   
3) Переходим на корень сайта, мы попадаем в панель тестирования нашего api, здесь можно добавлять, редактировать и выолнять запросы.   

### Ps
Основной код находится в папке app/http/, namespace company содержит модели и парсер, api\CompanyController - основной котроллер и т д  
В коде не учитана ситуация филтрации дублей компании по инн и режима поиска и соотвестенно индекса по полям inn и kpp (не успел и в задании не было, при желании можно доработать)   




