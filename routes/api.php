<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route ;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function() {
    Route::post("/company/create", "CompanyController@create") ;
    Route::get("/company/read", "CompanyController@read") ;
    Route::get("/company/retrieve/{id}", "CompanyController@retrieve") ;
    Route::post("/company/update/{id}", "CompanyController@update") ;
    Route::post("/company/destroy/{id}", "CompanyController@destroy") ;
    Route::post("/company/compose", "CompanyController@compose") ;
    Route::post("/company/remove/{id}", "CompanyController@remove") ;
    Route::get("/company/export", "CompanyController@export") ;
    Route::post("/company/import", "CompanyController@import") ;
    Route::any("/company/{command}", "CompanyController@nonFindCommand") ;
}) ;





