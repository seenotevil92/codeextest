require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';

window.Vue.use(VueRouter);

import CompaniesSelect from './components/companies/CommandSelect.vue';
Vue.component("companies-select", CompaniesSelect);



import CommandRead from './components/companies/CommandRead.vue';
import CommandEmpty from './components/companies/CommandEmpty.vue';
import CommandUpdate from './components/companies/CommandUpdate.vue';
import CommandCreate from './components/companies/CommandCreate.vue';
import CommandRetrieve from './components/companies/CommandRetrieve.vue';
import CommandRemove from './components/companies/CommandRemove.vue';
import CommandDestroy from './components/companies/CommandDestroy.vue';
import CommandCompose from './components/companies/CommandCompose.vue';
import CommandExport from './components/companies/CommandExport.vue';
import CommandImport from './components/companies/CommandImport.vue';

const routes = [
    { path: '/', component: CommandEmpty },
    { path: '/read', component: CommandRead },
    { path: '/create', component: CommandCreate },
    { path: '/update', component: CommandUpdate },
    { path: '/retrieve', component: CommandRetrieve },
    { path: '/remove', component: CommandRemove },
    { path: '/destroy', component: CommandDestroy },
    { path: '/compose', component: CommandCompose },
    { path: '/export', component: CommandExport },
    { path: '/import', component: CommandImport },
];

const router = new VueRouter({ routes });
const app = new Vue({ router }).$mount('#app');