<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Script -->
</head>

<body>
    <div class="container" id="app">
            <div class="row">
                <div class="col-md-12 col-md-offset-2">
                    <h2 style="padding-top: 20px;" class="text-center">Testing api</h2>
                    <div class="panel panel-default">
                        <div class="panel-heading">Commands</div>
                        <companies-select></companies-select>

                        <div class="panel-body table-responsive">
                            <router-view name="companiesIndex"></router-view>
                            <router-view></router-view>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>

</body>