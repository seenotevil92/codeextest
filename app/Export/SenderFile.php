<?php

namespace App\Export ;

class SenderFile extends Resource {

    private $path ;
    private $chunkBytes  ;

    public function __construct(string $path, string $mode = "rb+", int $chunkBytes = 1024)
    {
        $this->path = $path ;
        $this->chunkBytes = $chunkBytes ;
        parent::__construct($mode) ;
    }

    public function close()
    {
        if(is_resource($this->resource)) {
            fclose($this->resource) ;
        }
    }

    public function open()
    {
        $this->resource = fopen($this->path, $this->mode) ;
    }

    public function send($name = null)
    {
        if (ob_get_level()) {
            ob_end_clean();
        }
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='  . $name ? basename($this->path) : $name);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($this->path));
        fseek($this->resource, 0) ;

        while (!feof($this->resource)) {
            print fread($this->resource, 1024);
        }
        $this->close() ;
        exit(1) ;
    }

}