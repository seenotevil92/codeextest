<?php


namespace App\Export ;

class CsvFile extends Resource {

    const DEFAULT_DELIMETR = "," ;
    private $delimetr ;
    private $path ;
    /**
     * CsvFile constructor.
     * @param string $path
     * @param string $mode
     * @param string $delimetr
     */
    public function __construct(string $path, string $mode = "w+", string $delimetr = self::DEFAULT_DELIMETR)
    {
        $this->delimetr = $delimetr ;
        $this->path = $path ;
        parent::__construct($mode) ;
    }


    public function readLine()
    {
        return fgetcsv($this->resource) ;
    }

    public function addLine(array $array)
    {
        fputcsv($this->resource, $array, $this->delimetr);
    }

    public function addLineList(array $array)
    {
        foreach ($array as $lineArray) {
            $this->addLine($lineArray) ;
        }
    }

    public function each(callable $callback, int $chunkNum = 1, $assoc = true)
    {
        $columns = [] ;
        $num = 0;
        $result = [] ;
        while (($data = fgetcsv($this->resource, $this->delimetr)) !== false) {
            $num++ ;
            if($assoc && $num == 1) {
                $columns = $data ;
                continue ;
            }

            if($assoc) {
                $nData = [] ;
                foreach ($columns as $columnNum => $columnValue) {
                    $nData[$columnValue] = $data[$columnNum] ?? "" ;
                }
                $data = $nData ;
            }
            $result[] = $data ;
            if($chunkNum == 1) {
                call_user_func($callback, $result) ;
                $result = [] ;
                $num = 0;
            }
        }
    }



    public function close()
    {
        if(is_resource($this->resource)) {
            fclose($this->resource) ;
        }
    }

    public function open()
    {
        $this->resource = fopen($this->path, $this->mode) ;
    }
}