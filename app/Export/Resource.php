<?php

namespace App\Export ;

abstract class Resource {

    protected $resource ;
    protected $mode ;

    public function __construct(string $mode)
    {
        $this->mode = $mode ;
        $this->open() ;
    }

    public function __destruct()
    {
        $this->close() ;
    }

    abstract public function close() ;
    abstract public function open() ;



}