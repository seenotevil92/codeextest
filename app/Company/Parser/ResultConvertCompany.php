<?php


namespace App\Company\Parser ;

use App\Company\Models\Company;

class ResultConvertCompany {

    public function convert2Company(Result $result, Company $company = null) : Company
    {
        $company = $company instanceof Company ? $company : new Company() ;
        $company->name = $result->getName() ;
        $company->inn = $result->getInn() ;
        $company->ogrn = $result->getOgrn() ;
        $company->kpp = $result->getKpp() ;
        $company->address = $result->getAddress() ;
        $company->fio = $result->getDirectorFio() ;
        return $company ;
    }

}