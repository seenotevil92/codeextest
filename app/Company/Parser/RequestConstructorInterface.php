<?php

namespace App\Company\Parser ;

interface RequestConstructorInterface {
    public function asArray() : array ;
}