<?php

namespace App\Company\Parser\DaData ;

use App\Company\Parser\ParserException;
use App\Company\Parser\Result;

class Searcher {

    /**
     * @var RequestConstructor
     */
    private $requestConstructor ;
    private $client ;
    private $config ;


    /**
     * Searcher constructor.
     * @param null $config
     * @param \GuzzleHttp\Client $client
     * @throws ParserException
     */
    public function __construct(array $config = [], \GuzzleHttp\Client $client)
    {
        $this->client = $client ;
        $this->config = $config ;
        if(empty($this->config['api-key'])) {
            throw new ParserException($this->requestConstructor, "Api key is null") ;
        }
    }

    public function setRequestConstructor(RequestConstructor $requestConstructor) : self
    {
        $this->requestConstructor = $requestConstructor->setApiKey($this->config['api-key']) ;
        return $this ;
    }

    /**
     * @return array
     * @throws ParserException
     */
    public function findCompany() : Result
    {
        if(is_null($this->requestConstructor)) {
            throw new \LogicException("Request constructor is null" ) ;
        }
        $request = $this->requestConstructor->request() ;
        try {

            $response = $this->client->request($request->getMethod(), $request->getUri(), [
                'verify' => false,
                'headers' => $request->getHeaders(),
                'body' => $request->getBody()
            ]) ;

            if(empty($response->getBody()) || $response->getStatusCode() !== 200) {
                throw new ParserException($this->requestConstructor, "Result body is null") ;
            }
            return $this->parseResult($response->getBody()) ;

        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            throw new ParserException($this->requestConstructor, "Guzzle http error {$e->getMessage()}", $e->getCode(), $e) ;
        }

    }

    /**
     * @param string $body
     * @return Result
     * @throws ParserException
     */
    private function parseResult(string $body) : Result
    {
        $json = json_decode($body, true) ;
        if(empty($json)) {
            throw new ParserException($this->requestConstructor, "Json decode result error ") ;
        }

        $suggestion = $json['suggestions'][0] ?? [] ;
        if(empty($suggestion) || empty($suggestion['data'])) {
            throw new ParserException($this->requestConstructor,"Suggestions is null result error ") ;
        }

        $result = new Result() ;
        $result->setName($suggestion['value']) ;
        $result->setInn($suggestion['data']['inn']) ;
        $result->setOgrn($suggestion['data']['ogrn'] ?? "") ;
        $result->setKpp($suggestion['data']['kpp'] ?? "") ;
        $result->setAddress($suggestion['data']['address']['value'] ?? "") ;
        $result->setDirectorFio($suggestion['data']['management']["name"] ?? "") ;
        return $result ;
    }




}