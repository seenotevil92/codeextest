<?php


namespace App\Company\Parser\DaData ;

use App\Company\Parser\RequestConstructorInterface;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;

class RequestConstructor implements RequestConstructorInterface {

    const TYPE_JSON = "application/json" ;
    const TYPE_XML = "application/xml" ;
    const URI = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/party" ;


    private $type ;
    private $apiKey ;
    private $query ;
    private $uri ;

    public function __construct()
    {
        $this->type = self::TYPE_JSON ;
        $this->uri = self::URI ;
    }

    public function setType($type) : self
    {
        $this->type = $type ;
        return $this ;
    }

    public function setUri(string $uri) : self
    {
        $this->uri = $uri ;
        return $this ;
    }

    public function setApiKey(string $apiKey) : self
    {
        $this->apiKey = $apiKey ;
        return $this ;
    }

    public function setQuery(string $query) : self
    {
        $this->query = $query ;
        return $this ;
    }

    public function request() : RequestInterface
    {
        $headers = [
            "Content-Type" => $this->type,
            "Accept" => $this->type,
            "Authorization" => "Token {$this->apiKey}"
        ] ;

        $post = [
            'query' => $this->query
        ] ;

        return new Request('POST', $this->uri, $headers, json_encode($post)) ;
    }

    public function asArray() : array
    {
        return [
            'type' => $this->type,
            'apiKey' => $this->apiKey,
            'query' => $this->query
        ] ;
    }




}