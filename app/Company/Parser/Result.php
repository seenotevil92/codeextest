<?php

namespace App\Company\Parser ;

class Result {

    private $inn = "" ;
    private $kpp = "" ;
    private $ogrn = "" ;
    private $address = "" ;
    private $directorFio = "" ;
    private $name = "" ;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
    /**
     * @return string
     */
    public function getInn(): string
    {
        return $this->inn;
    }

    /**
     * @param string $inn
     */
    public function setInn(string $inn): void
    {
        $this->inn = $inn;
    }

    /**
     * @return string
     */
    public function getKpp(): string
    {
        return $this->kpp;
    }

    /**
     * @param string $kpp
     */
    public function setKpp(string $kpp): void
    {
        $this->kpp = $kpp;
    }

    /**
     * @return string
     */
    public function getOgrn(): string
    {
        return $this->ogrn;
    }

    /**
     * @param string $ogrn
     */
    public function setOgrn(string $ogrn): void
    {
        $this->ogrn = $ogrn;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getDirectorFio(): string
    {
        return $this->directorFio;
    }

    /**
     * @param string $directorFio
     */
    public function setDirectorFio(string $directorFio): void
    {
        $this->directorFio = $directorFio;
    }





}