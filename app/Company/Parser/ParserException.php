<?php


namespace App\Company\Parser ;

use App\Company\Parser\RequestConstructorInterface;
use Throwable;

class ParserException extends \Exception {

    private $requestConstructor ;


    public function __construct(RequestConstructorInterface $requestConstructor, string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->requestConstructor = $requestConstructor ;
        parent::__construct($message, $code, $previous);
    }

}