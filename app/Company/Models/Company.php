<?php

namespace App\Company\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * @property $id int
 * @property $gid string
 * @property $created_at string
 * @property $updated_at string
 * @property $name string
 * @property $ogrn string
 * @property $inn string
 * @property $kpp string
 * @property $address string
 * @property $fio string
 * @property $date_removed int
 *
 */
class Company extends Model
{
    const REMOVED_ON = 1 ;
    const REMOVED_OFF = 0 ;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $fillable = [
        'gid', 'name', 'ogrn', 'inn', 'kpp', 'address', 'fio', 'id'
    ] ;


    public function remove()
    {
        $this->date_removed = self::REMOVED_ON ;
        $this->save() ;
    }

    public function findFillField(array $array = [])
    {
        for($i = 0; $i < count($array); $i++) {
            $field = $array[$i] ;
            $value = $this->$field ;
            if(!empty($value)) {
                return $value ;
            }
        }
    }


}
