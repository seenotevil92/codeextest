<?php


namespace App\Http\Controllers\Api ;

class ResponseError extends ResponseJson
{

    public function __construct($data)
    {
        parent::__construct($status = self::STATUS_ERROR, ["error" => $data]);
    }


}