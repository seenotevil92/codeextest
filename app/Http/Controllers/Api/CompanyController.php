<?php

namespace App\Http\Controllers\Api;

use App\Company\Models\Company;
use App\Company\Parser\DaData\RequestConstructor;
use App\Company\Parser\DaData\Searcher;
use App\Company\Parser\ParserException;
use App\Company\Parser\ResultConvertCompany;
use App\Export\CsvFile;
use App\Export\SenderFile;
use App\Http\Requests\CompanyStoreRequest;
use App\Http\Resources\CompanyResource;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class CompanyController extends Controller
{
    const ALLOWED_CSV_PARAMS = ["name", "inn", "kpp", "ogrn", "fio", "address"];

    public function create(CompanyStoreRequest $request)
    {
        $errors = $request->validate() ;
        if($errors) {
            return $this->response(new ResponseError($errors)) ;
        }

        $company = new Company();
        $company->fill($request->all());
        $company->gid = Str::uuid() ;

        $result = $company->save();

        if(!$result) {
            return $this->response(new ResponseError('Save error')) ;
        }
        return $this->response(new ResponseSuccess());
    }

    public function read()
    {
        return $this->response(
            new ResponseSuccess( CompanyResource::collection(
                Company::where(["date_removed" => Company::REMOVED_OFF])->get())
            )
        ) ;
    }

    public function retrieve($id)
    {
        $company = Company::where(["id" => $id, "date_removed" => Company::REMOVED_OFF])->first();
        if (is_null($company) || $company->id == 0) {
            return $this->response(new ResponseError("Not found company by id")) ;
        }
        return $company ;
    }

    public function update(CompanyStoreRequest $request, int $id)
    {
        $company = Company::where(["id" => $id, "date_removed" => Company::REMOVED_OFF])->first();
        if (is_null($company)) {
           return $this->response(new ResponseSuccess("Not found company by id")) ;
        }

        $errors = $request->validate() ;
        if($errors) {
            return $this->response(new ResponseError($errors)) ;
        }

        $company->fill($request->all());
        $success = $company->save();
        if(!$success) {
            return $this->response(new ResponseError("Save failed")) ;
        }
        return $this->response(new ResponseSuccess()) ;
    }


    public function destroy(int $id)
    {
        $company = Company::find($id);
        if (is_null($company)) {
            return $this->response(new ResponseError("Not found company by id")) ;
        }

        $success = $company->delete();
        if (!$success) {
            return $this->response(new ResponseError("Delete company id")) ;
        }
        return $this->response(new ResponseJson(ResponseJson::STATUS_SUCCESS)) ;
    }

    public function compose(Request $request)
    {
        $company = new Company() ;
        $company->fill($request->all());

        $query = $company->findFillField(['inn', 'ogrn', 'name', 'fio']);
        if (empty($query)) {
            return $this->response(new ResponseError("Not find field for query")) ;
        }

        $config = include public_path() . '/../config/dadata.php';
        try {
            $result = (new Searcher($config, new Client()))->setRequestConstructor((new RequestConstructor())->setQuery($query))->findCompany();
            $company = (new ResultConvertCompany())->convert2Company($result);
            return $this->response(new ResponseSuccess($company)) ;

        } catch (ParserException $e) {
            return $this->response(new ResponseError("ParseError")) ;
        }

    }

    public function remove(int $id)
    {
        $company = Company::where(["id" => $id, "date_removed" => Company::REMOVED_OFF])->first();
        if(is_null($company)) {
            return $this->response(new ResponseError("Not find company")) ;
        }
        $success = $company->remove();
        if($success) {
            return $this->response(new ResponseError( "Remove failed")) ;
        }

        return $this->response(new ResponseSuccess()) ;
    }

    public function nonFindCommand()
    {
        return $this->response( new ResponseError("not find command")) ;
    }


    public function export(Request $request)
    {
        $limit = $request->all()['limit'] ?? 1000 ;
        $companies = Company::where(['date_removed' => Company::REMOVED_OFF])->take($limit)->get() ;

        try {
            $tmpFile = tempnam(storage_path("/tmp"), "Company_Csv") ;
            $csvFile = new CsvFile($tmpFile) ;
            $i = 0 ;
            foreach ($companies as $company) {
                if($i == 0)
                    $csvFile->addLine(self::ALLOWED_CSV_PARAMS) ;

                $i++ ;
                $csvFile->addLine(array_filter_by_kyes($company->toArray(), self::ALLOWED_CSV_PARAMS)) ;
            }
            (new SenderFile($tmpFile))->send("export.csv") ;
        } catch (\Throwable $e) {
            echo $e->getMessage() ;
            return $this->response(new ResponseError("Error export file")) ;
        }

    }


    public function import(Request $request)
    {
        $file = $request->file('import_file') ;
        if(!$file){
            return $this->response(new ResponseError("Empty field file")) ;
        }

        $path = $file->getRealPath() ;
        try {
            $csvFile = new CsvFile($path, "r") ;
            $csvFile->each(function($arrayList){
                foreach ($arrayList as $modelArray) {
                    $company = new Company();
                    $company->fill(array_intersect_key(self::ALLOWED_CSV_PARAMS, $modelArray));
                    $company->gid = Str::uuid() ;
                    $company->save();
                }
            }) ;
        } catch (\Throwable $e) {
            return $this->response(new ResponseError("Error export file")) ;
        }

    }


    private function response(ResponseJson $responseJson)
    {
        return response()->json($responseJson->getData(), $responseJson->getStatus()) ;
    }
}
