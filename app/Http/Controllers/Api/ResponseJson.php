<?php


namespace App\Http\Controllers\Api ;

class ResponseJson {

    const STATUS_SUCCESS = 200 ;
    const STATUS_ERROR  = 404 ;

    protected $status ;
    protected $data = [] ;

    public function __construct(int $status, $data = [])
    {
        $this->status = $status ;
        $this->data = $data ;
    }

    public function getData()
    {
        return $this->data ;
    }

    public function getStatus() : int
    {
        return $this->status ;
    }

}