<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'gid' => $this->gid,
            'name' => $this->name,
            'ogrn' => $this->ogrn,
            'inn' => $this->inn,
            'kpp' => $this->kpp,
            'address' => $this->address,
            'fio' => $this->fio
        ];
    }
}
