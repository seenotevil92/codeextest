<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class CompanyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }


    public function validate()
    {
        $validator = Validator::make($this->all(), [
            'name' => 'required|max:255',
            'fio' => 'required|max:255',
            'inn' => 'required|max:12',
            'kpp' => 'required|max:9',
            'address' => 'required|max:255',
        ]) ;
        return $validator->fails() > 0 ? $validator->errors() : []  ;
    }
}
